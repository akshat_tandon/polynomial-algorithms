#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define MAX 50
#define SUCCESS     1
#define NOTSUCCESS  0

struct Polynomial{
	int deg;
	int cof[MAX];
};
typedef struct Polynomial Polynomial;

void init_polynomial(Polynomial* p, int degree){
	p->deg = degree;
	int i;
	for(i=0;i<=degree;i++){
		p->cof[i] = 0;
	}
}

void print_polynomial(Polynomial* p){
	int i;
	for(i=0;i<35;i++)
		printf("-");
	printf("\n");
	printf("Degree : %d\n",p->deg);
	printf("%d",p->cof[0]);
	for(i=1; i <= p->deg; i++){
		printf(" + %dx^%d",p->cof[i],i);
	}
	printf("\n");
	for(i=0;i<35;i++)
		printf("-");
	printf("\n");
}

int polynomial_division(Polynomial *g, Polynomial *h, Polynomial *q, Polynomial *r, int p){
	/*	
	 *	Given two polynomials g,h belonging to ring of polynomials Zp[X]
	 *	Calculates two polynomials q,r st g = hq + r
	 *	Memory for all the polynomials must be allocated and freed in the calling method.
	 *	This method neither allocates nor frees memory for Polynomials.That is the reponsibility of
	 *	the polynomial class.
	 */
	int t,k,l,i,j;
	k = g->deg + 1;
	l = h->deg + 1;
	t = find_zp_inverse(p,h->cof[l-1]);
	if(t == NOTSUCCESS){
		printf("Not able to find zp inverse of %d\n",h->cof[h->deg]);
		return NOTSUCCESS;
	}
	//printf("inverse is -- %d\n",t);
	init_polynomial(r,l-2);
	for(i=0;i<=k-1;i++){
		r->cof[i] = g->cof[i];
	}
	init_polynomial(q,k-l);
	for(i=k-l;i>=0;i--){
			q->cof[i] = ( t*(r->cof[i+l-1]) )%p;
			for(j=0;j<=l-1;++j){
				r->cof[i+j] = (r->cof[i+j] - ( (q->cof[i]) * (h->cof[j])  )%p ) %p;
			}
	}
	return SUCCESS;
}

int polynomial_gcd(Polynomial* g, Polynomial* h, Polynomial* d, int p){
	int lead_cof,inv;
	Polynomial *r = malloc(sizeof(Polynomial));
	Polynomial *rd = malloc(sizeof(Polynomial));
	Polynomial *rdd = malloc(sizeof(Polynomial));
	Polynomial *tmp = malloc(sizeof(Polynomial));
	copy_polynomial(r,g);
	copy_polynomial(rd,h);
	while(!check_zero_polynomial(rd)){
		polynomial_division(r,rd,tmp,rdd,p);
		copy_polynomial(r,rd);
		copy_polynomial(rd,rdd);
	}
	lead_cof = get_lc(r);
	inv = find_zp_inverse(p, lead_cof);
	polynomial_scalar_mulitply(r,inv,p);
	copy_polynomial(d,r);
	free(r);
	free(rd);
	free(rdd);
	free(tmp);
	return SUCCESS;
}

int copy_polynomial(Polynomial* a, Polynomial* b){
	int i;
	init_polynomial(a,b->deg);
	for(i=0;i<= a->deg; i++){
		a->cof[i] = b->cof[i];
	}
	return SUCCESS;
}

int find_zp_inverse(int p,int a){
	int i;
	for(i=0; i<p; i++){
		if( (i*a)%p == 1)return i;
	}
	return NOTSUCCESS;
}

int get_lc(Polynomial* a){
	return a->cof[a->deg];
}

int polynomial_scalar_mulitply(Polynomial* a,int s,int p){
	int i;
	for(i=0;i< a->deg; i++){
		a->cof[i] = (a->cof[i] * s) % p;
	}
}

int check_zero_polynomial(Polynomial* pol){
	int i;
	for(i=0; i< pol->deg ; i++){
		if( pol->cof[i] != 0)return 0;
	}
	return 1;
}

main(){
	int degree,i,*ptr,p,t,k,l,j,lead_cof,inv;
	Polynomial *g = malloc(sizeof(Polynomial));
	Polynomial *h = malloc(sizeof(Polynomial));	
	Polynomial *d = malloc(sizeof(Polynomial));
	printf("Euclidean Algorithm for Polynomials\n");
	printf("Enter p of Zp[X] : ");
	scanf("%d",&p);
	printf("Enter polynomial g belonging to Z%d[X] , deg(g) ≥ deg(h):\n",p);
	printf("Enter k (degree+1): ");
	scanf("%d",&k);
	//k-1 is the degree
	init_polynomial(g,k-1);
	printf("Enter its coffecients\n");
	for(i=0;i<= g->deg;i++){
		scanf("%d",&g->cof[i]);
	}
	printf("Enter polynomial h belonging to Z%d[X] , deg(g) ≥ deg(h):\n",p);
	printf("Enter l (degree+1): ");
	scanf("%d",&l);
	//l-1 is the degree
	init_polynomial(h,l-1);
	printf("Enter its coffecients\n");
	for(i=0;i<= h->deg;i++){
		scanf("%d",&h->cof[i]);
	}
	//print_polynomial(g);
	//print_polynomial(h);
	//----//
	//int polynomial_gcd(Polynomial* g, Polynomial* h, Polynomial* d, int p)
	polynomial_gcd(g,h,d,p);
	printf("GCD of the given two polynomials is : \n");
	print_polynomial(d);
	free(g);
	free(h);
	free(d);
}


