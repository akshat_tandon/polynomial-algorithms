#include <stdio.h>
#include <math.h>
main(){
	int a,b,r,rd,d,s,sd,t,td,rdd,q,old_s,old_t;
	printf("Extended Euclidean Algorithm\n");
	printf("Enter a: ");
	scanf("%d",&a);
	printf("Enter b: ");
	scanf("%d",&b);
	r = a;
	rd = b;
	s = 1;
	sd = 0;
	t = 0;
	td = 1;
	while(rd != 0){
		q = floor((double)r/rd);
		//printf("q is %d\n",q);
		rdd = r % rd;
		r = rd;
		rd = rdd;
		old_s = s;
		s = sd;
		sd = old_s - sd*q;
		old_t = t;
		t = td;
		td = old_t - td*q;
		//printf("sd = %d and td = %d\n",sd,td);		
	}
	d = r;
	printf("Gcd(%d,%d) = %d such that %d.%d + %d.%d = %d\n",a,b,d,a,s,b,t,d);
}